/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.parg;

import com.sun.deploy.uitoolkit.impl.fx.HostServicesFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Paint;

/**
 *
 * @author PABLO
 */
public class ImageEmbeddingController implements Initializable {

    public static final String LBL_CSS = "-fx-border-radius: 15; -fx-border-width: 3; -fx-border-style: dashed; -fx-background-color: #eee;";
    public static final String LBL_TEXT = "Arraste as imagens aqui";
    public static final String LBL_DRAG = "Solte as imagens aqui";

    @FXML
    private Label lblImagens;

    @FXML
    private TextArea txtCodigo;
    
    @FXML
    private CheckBox cbxCDATA;

    @FXML
    private void onDragDetected(MouseEvent event) {
        // muda o css
        lblImagens.setStyle(LBL_CSS + "-fx-border-color: #f00;");
        lblImagens.setTextFill(Paint.valueOf("#990012"));

        // muda o texto
        lblImagens.setText(LBL_DRAG);
    }

    @FXML
    private void onDragOver(DragEvent event) {
        Dragboard db = event.getDragboard();
        if (db.hasFiles()) {
            // muda o css
            lblImagens.setStyle(LBL_CSS + "-fx-border-color: #f00;");
            lblImagens.setTextFill(Paint.valueOf("#990012"));

            // muda o texto
            lblImagens.setText(LBL_DRAG);

            // aceita a transferência
            event.acceptTransferModes(TransferMode.COPY);
        } else {
            event.consume();
        }
    }

    @FXML
    private void onDragDropped(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;

        // verifica se tem arquivos
        if (db.hasFiles()) {
            success = true;
            String filePath = "";

            for (File file : db.getFiles()) {
                try {
                    filePath = file.getAbsolutePath();
                    txtCodigo.appendText("--------- " + filePath + " ---------\n\n");
                    
                    // verifica se é para incluir CDATA
                    if (cbxCDATA.isSelected()) {
                        txtCodigo.appendText("\t<![CDATA[\"" + encryptBase64(Files.readAllBytes(Paths.get(filePath))) + "\"]]>\n\n ");
                    } else {
                        txtCodigo.appendText("\t" + encryptBase64(Files.readAllBytes(Paths.get(filePath))) + "\n\n ");
                    }
                } catch (IOException ex) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Ocorreu um erro!");
                    alert.setHeaderText("Ocorreu codificar o arquivo " + filePath + "!");
                    alert.setContentText(ex.getMessage());
                }
            }
        }

        // retorna
        event.setDropCompleted(success);
        event.consume();
    }

    @FXML
    private void onDragExited(DragEvent event) {
        // muda o css
        lblImagens.setStyle(LBL_CSS + "-fx-border-color: #bbb;");
        lblImagens.setTextFill(Paint.valueOf("#777777"));

        // muda o texto
        lblImagens.setText(LBL_TEXT);
    }
    
    @FXML
    private void hiperlinkAction(ActionEvent event) {
        HostServicesFactory.getInstance(JRImageEmbedding.getInstance()).showDocument("http://www.parg.com.br/");
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    /**
     * Criptografa conjunto de byte para um texto em base64
     *
     * @param bytes
     * @return
     */
    public static String encryptBase64(byte[] bytes) {
        return Base64.getEncoder().encodeToString(bytes);
    }

}
