/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.parg;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author PABLO
 */
public class JRImageEmbedding extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        // armazena no Singleton
        JRImageEmbeddingHolder.INSTANCE = this;
        
        // carrega o formulário
        Parent root = FXMLLoader.load(getClass().getResource("ImageEmbedding.fxml"));
        
        // configura a cena
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setTitle("JRImageEmbedding");
        stage.getIcons().add(new Image("br/com/parg/icons/xml.png"));
        stage.setMaximized(false);
        stage.setResizable(false);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static JRImageEmbedding getInstance() {
        return JRImageEmbeddingHolder.INSTANCE;
    }
    
    private static class JRImageEmbeddingHolder {
        public static JRImageEmbedding INSTANCE;
    }
    
}
