# JRImageEmbedding
Aplicativo em JavaFX que transforma uma imagem em base64 e a prepara para embutir nos relatórios do JasperReport.

![Screenshot do teste da ferramenta](http://imagens.parg.com.br/jrembeddingimage.gif)

Depois de transformado em base64 você somente precisa armazenar a saída em uma variável no Jaspersoft Studio e inserir um campo **Image** e colocar a seguinte expressão nele:

```java
new ByteArrayInputStream(Base64.decodeBase64($V{SuaImagemBase64}.getBytes()))
```

# Aviso
Este projeto serve somente como ferramenta auxiliar ao JasperReport, não é um plugin do *TIBCO Jaspersoft Studio*.